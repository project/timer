

Description
-----------
Timer defines a CCK field type for a timer, which can be started, stopped and 
reset via timer button controls or workflow actions. Timer fields can be shown 
in real-time with DHTML or only updated on page refresh. Timer button controls 
can be restricted by both field setting and Use timer controls access 
permissions.


Prerequisites
-------------
The Content Contruction Kit content.module is enabled. Optionally, 
workflow.module and actions.module enabled.


Installation
------------
To install, copy the timer directory and all its contents to your modules
directory.


Configuration
-------------
1. enable the module: admin/build/modules
2. set access permissions for roles: admin/user/access
3. add the Timer field widget to your node type
4. (optional) create Start/Stop/Reset actions for your node type
5. (optional) associate actions with workflow attached to your node type


Bugs/Features/Patches:
----------------------
If you want to report bugs, feature requests, or submit a patch, please do so 
at the project page on the Drupal web site.
http://drupal.org/project/timer


Todo List:
----------
* elapsed timer with expiration
* countdown timer with expiration


Author
------
John Money
gestaltware, inc.
http://gestaltware.com

Module development sponsored by LifeWire, a subsidiary of The New York Times 
Company.
http://www.lifewire.com
