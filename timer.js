
function timer(startTime, obj){
  this.currentTime=new Date().getSeconds();
  this.startTime=this.currentTime - startTime;
  this.start(obj);
}

timer.prototype.start=function(obj) {
  var oneMinute=60; var oneHour=3600; var oneDay=86400; var oneYear=31536000;
  var thisobj=this;
  this.currentTime+=1;
  var timediff=(this.currentTime-this.startTime);
  var years=Math.floor(timediff/oneYear);
  timediff -= years*oneYear;
  var days=Math.floor(timediff/oneDay);
  timediff -= days*oneDay;
  var hours=Math.floor(timediff/oneHour);
  timediff -= hours*oneHour;
  var minutes=Math.floor(timediff/oneMinute);
  var seconds=Math.floor((timediff-minutes*oneMinute));
  var result = '';
  if (years > 0) {
    result += (years == 1) ? years + " year " : years + " years ";
  }
  if (years || days > 0) {
    result += (days == 1) ? days + " day " : days + " days ";
  }
  if (years || days || hours > 0) {
    result += (hours == 1) ? hours + " hour " : hours + " hours ";
  }
  if (years || days || hours || minutes > 0) {
    result += (minutes == 1) ? minutes + " minute " : minutes + " minutes ";
  }
  if (years || days || hours || minutes || seconds > 0) {
    result += (seconds == 1) ? seconds + " second " : seconds + " seconds ";
  }
  var htmlcontainer=document.getElementById(obj);
  htmlcontainer.innerHTML=result;
  setTimeout(function(){thisobj.start(obj)}, 1000);
}
